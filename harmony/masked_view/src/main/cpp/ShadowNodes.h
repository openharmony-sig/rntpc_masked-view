// Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

#ifndef HARMONY_MASKEDVIEW_SRC_MAIN_CPP_SHADOWNODES_H
#define HARMONY_MASKEDVIEW_SRC_MAIN_CPP_SHADOWNODES_H

#include <react/renderer/components/view/ConcreteViewShadowNode.h>
#include <react/renderer/components/view/ViewShadowNode.h>
#include <jsi/jsi.h>

namespace facebook {
namespace react {

    JSI_EXPORT extern const char MaskedViewName[];

    /*
     * `ShadowNode` for <RNCMaskedView> component.
     */
    using MaskedViewShadowNode = ConcreteViewShadowNode<MaskedViewName, ViewProps, ViewEventEmitter>;

} // namespace react
} // namespace facebook
#endif