// Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

#pragma once

#include <react/renderer/core/ConcreteComponentDescriptor.h>
#include "ShadowNodes.h"

namespace facebook {
namespace react {

class MaskedViewDescriptor final
    : public ConcreteComponentDescriptor<MaskedViewShadowNode> {
  public:
    MaskedViewDescriptor(ComponentDescriptorParameters const &parameters)
        : ConcreteComponentDescriptor(parameters) {}
};

} // namespace react
} // namespace facebook
