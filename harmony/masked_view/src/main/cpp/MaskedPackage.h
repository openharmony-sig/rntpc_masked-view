// Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

#ifndef IMAGECROPPICKERPACKAGE_H
#define IMAGECROPPICKERPACKAGE_H


#include "MaskedViewDescriptor.h"
#include "RNOH/BaseComponentJSIBinder.h"
#include "RNOH/BaseComponentNapiBinder.h"
#include "RNOH/Package.h"
#include "MaskedComponentInstance.h"

namespace rnoh {

    class MaskedPackageComponentInstanceFactoryDelegate : public ComponentInstanceFactoryDelegate {
    public:
        using ComponentInstanceFactoryDelegate::ComponentInstanceFactoryDelegate;

        ComponentInstance::Shared create(ComponentInstance::Context ctx) override {
            if (ctx.componentName == "RNCMaskedView") {
                return std::make_shared<MaskedComponentInstance>(std::move(ctx));
            }
            return nullptr;
        }
    };

    class MaskedPackage : public Package {
    public:
        MaskedPackage(Package::Context ctx) : Package(ctx) {}

        ComponentInstanceFactoryDelegate::Shared createComponentInstanceFactoryDelegate() override {
            return std::make_shared<MaskedPackageComponentInstanceFactoryDelegate>();
        }

        std::vector<facebook::react::ComponentDescriptorProvider> createComponentDescriptorProviders() override {
            return {facebook::react::concreteComponentDescriptorProvider<facebook::react::MaskedViewDescriptor>()};
        }

        ComponentJSIBinderByString createComponentJSIBinderByName() override {
            return {{"RNCMaskedView", std::make_shared<BaseComponentJSIBinder>()}};
        }

        ComponentNapiBinderByString createComponentNapiBinderByName() override {
            return {{"RNCMaskedView", std::make_shared<BaseComponentNapiBinder>()}};
        }
    };
} // namespace rnoh

#endif //IMAGECROPPICKERPACKAGE_H
