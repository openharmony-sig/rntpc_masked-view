import {AppRegistry, View, Text} from 'react-native';
import {name as appName} from './app.json';
import App from './src/MaskedDemo';

AppRegistry.registerComponent(appName, () => App);