import { StyleSheet, Text, View } from 'react-native';
import MaskedView from '@react-native-masked-view/masked-view';

const MaskedDemo = () => {
    return (
        <MaskedView
            style={styles.maskedView}
            maskElement={
                <View style={styles.maskElementView}>
                    <Text style={styles.maskElementText}>Basic Mask</Text>
                </View>
            }
        >
            <View style={{ width:'100%', height: 40, backgroundColor: '#fe4b83' }} />
            <View style={{ width:'100%', height: 40, backgroundColor: '#F5DD90' }} />
        </MaskedView>
    )
}

const styles = StyleSheet.create({
    maskedView: {
        width:'100%',
        height:'40%'
    },
    maskElementView: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        width:'100%',
        height:'100%',
        justifyContent: 'center',
    },
    maskElementText: {
        color: Colors.black,
        width:'100%',
        height:60,
        fontSize: 50,
        fontWeight: 'bold',
    },
    textView: {
        fontSize: 20,
        alignItems: 'center',
        width:'100%',
        height:40,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    text: {
        color: Colors.darkChestnut,
        fontSize: 8,
        width:'100%',
        height:40,
        fontVariant: ['small-caps'],
        fontWeight: 'bold',
    },
});

export default MaskedDemo